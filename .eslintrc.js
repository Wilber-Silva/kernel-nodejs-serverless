module.exports = {
    "root": true,
    "extends": "airbnb-base",
    "plugins": [
      "no-wildcard-postmessage",
      "no-unsanitized",
      "security",
      "scanjs-rules"
    ],
    "parserOptions": {
      "ecmaVersion": 2018,
      "sourceType": "script"
    },
    "env": {
      "browser": false,
      "node": true,
      "mongo": true
    },
    "rules": {
        "no-console": "off",
        "semi": "off",
        "max-len": ["error", 140],
        "no-extra-semi": "error",
        "no-unresolved": "off",
        "no-tabs": "off"
    }
};